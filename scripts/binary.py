from latex import *
from bits import *

def binary_to_unsigned_decimal_general(doc=vdoc):
    doc.line(r"Given a binary number as $b_{n}b_{n-1}\hdots b_{1}b_{0}$,")
    doc.line(r"we can convert it to decimal using the sum")
    doc.line(r"\[")
    doc.line(r"  \sum_{i=0}^{n} b_{i} \cdot 2^{i}")
    doc.line(r"\]")

def binary_to_unsigned_decimal(bits, doc=vdoc):
    doc.line(r"Converting $%s$ to decimal, we get" % str(bits))
    doc.line(r"\[")
    bits_reversed = bits[::-1]
    res = 0
    for i,bit in enumerate(bits_reversed):
        plus = "+" if i < len(bits_reversed)-1 else ""
        doc.line(r"  %d \cdot 2^{%d} %s" % (bit, i, plus))
        res += bit * 2**i
    doc.line(r"  = %d" % res)
    doc.line(r"\]")
    return res


def add_twos_complement(a, b, doc=vdoc):
    r = a.reverse() + b.reverse()
    return r.reverse()

def negate_twos_complement(bits, doc=vdoc):
    cs = "c" * (len(bits) + 1)
    def bits_table_line(bits):
        return " & ".join(map(lambda x: "$%s$" % x, str(bits)))
    normal = bits_table_line(bits)
    flipped = bits_table_line(bits.flip())
    one = Bits("1").reverse().width(len(bits)).reverse()
    one_line = bits_table_line(one)
    result_reversed = bits.flip().reverse() + Bits("1").reverse()
    result = result_reversed.reverse()
    result_line = bits_table_line(result)
    doc.line(r"\begin{table}[H]")
    doc.line(r"\centering")
    doc.line(r"\begin{tabular}{%s}" % cs)
    doc.line(r"       & %s \\" % normal)
    doc.line(r"$\neg$ & %s \\" % flipped)
    doc.line(r"$+$    & %s \\" % one_line)
    doc.line(r"\hline \\")
    doc.line(r"       & %s \\" % result_line)
    doc.line(r"\end{tabular}")
    doc.line(r"\end{table}")
    return result


def binary_to_twos_complement(bits, doc=vdoc):
    bits_reversed = bits[::-1]
    msb = bits_reversed.msb()
    doc.line(r"To convert a binary number to decimal in two's complement")
    doc.line(r"we first look at the MSB. ")
    doc.skip()
    doc.line(r"In case of our $%s$, the MSB is $%d$." % (str(bits), msb))
    if msb == True:
        doc.line(r"This means that the number is negative.")
        doc.line(r"Therefore we convert it to positive by flipping every bit and adding $1$ to it:")
        doc.skip()
        neg = negate_twos_complement(bits, doc)
        doc.skip()
        dec = binary_to_unsigned_decimal(neg, doc)
        doc.skip()
        doc.line(r"As we concluded the original number was negative, therefore this must be too.")
        doc.line(r"Therefore the final result is: $%d$" % (-dec))
    else:
        doc.line(r"This means that the number is positive.")
        doc.line(r"Now we simply convert it to decimal as usual:")
        return binary_to_unsigned_decimal(bits, doc)
     


def binary_to_hexidecimal(bits, doc=vdoc):
    bits_reversed = bits[::-1]
    def hex_(i):
        s = hex(i)
        r = s[2:].upper()
        return r
    chunks = bits_reversed.chunks(4)
    rev_chunks = chunks[::-1]
    doc.line(r"To convert binary to hexidecimal,")
    doc.line(r"we split the number up in chunks of 4: ")
    doc.skip()
    doc.line(r"%s" % " ".join(map(str, rev_chunks)))
    doc.skip()
    doc.line(r"Now, for every chunk we convert it to decimal,")
    doc.line(r"and numbers above 9 will be represented by the letters A-F.")
    doc.skip()
    result = []
    for c in chunks:
        rc = c.reverse()
        i = int(str(c), 2)
        h = hex_(i)
        doc.line(r"%s $\rightarrow$ %d $\rightarrow$ %s \\" % (str(c), i, h))
        result.append(h)
    rev_result = result[::-1]
    st = "".join(rev_result)
    doc.line(r"Finally, we can write our result as: %s" % st)
    return st


def decimal_to_binary(d, doc=vdoc):
    res = []
    n = d
    doc.line(r"To convert decimal to binary,")
    doc.line(r"we divide the number by 2,")
    doc.line(r"and find the quotient and remainder.")
    doc.line(r"Then we divide the quotient with 2 and continue until")
    doc.line(r"the result is zero")

    doc.line(r"\begin{table}[H]")
    doc.line(r"\centering")
    doc.line(r"\begin{tabular}{cccccc}")
    doc.line(r"$q_{old}$ & & &  & $q_{new}$ & Bit \\")
    doc.line(r"\hline \\")
    while n > 0:
        n2, b = divmod(n, 2)
        doc.line(r"%d & div & 2 & = & %d &    \\" % (n, n2))
        doc.line(r"%d & mod & 2 & = &    & %d \\" % (n, b))
        n = n2
        res.append(bool(b))
    doc.line(r"\end{tabular}")
    doc.line(r"\end{table}")

    bits = Bits(res).reverse()
    doc.line(r"The final result is: %s." % str(bits))
    return bits

def decimal_to_twos_complement(d, doc=vdoc):
    doc.line(r"When converting a decimal number to twos complement,")
    doc.line(r"we first look at the sign.")
    doc.line(r"In our case of $%d$," % d)
    if d == 0:
        doc.line(r"we know that the result is simple zero,")
        doc.line(r"so we don't need to look at the sign.")
    elif d > 0:
        doc.line(r"the number is positive,")
        doc.line(r"so we simply convert to binary unsigned:")
        bits = decimal_to_binary(d, doc)
        doc.line(r"Then we add a zero as the MSB to indicate that it is positive.")
        bits.pad_left(1)
        doc.line(r"The final result is: %s." % str(bits))
    else: # d < 0
        doc.line(r"the number is negative.")
        doc.line(r"Therefore we remove the sign and convert it to binary:")
        pos_bits = decimal_to_binary(-d, doc)
        doc.line(r"As we know that the result is negative, we negate it:")
        neg_bits = negate_twos_complement(pos_bits, doc)
        doc.line(r"Then we add a one as the MSB to indicate that it is negative.")
        res = neg_bits.pad_left(1, True)
        doc.line(r"The final result is: %s." % str(res))
        return res

# TODO: Implement elementary school multiplication with binary
# Note: Might not be implemented. Use Booth algorithm.
def binary_multiply(a, b, doc=vdoc):
    pass

# Implementation of booth algorithm described as a flow chart
# on page 39 in slide 2.
def binary_multiply_booth(a_, b_, doc=vdoc):
    l = max(len(a_), len(b_))
    a = Bits().width(l)                 # All zeros
    m = a_.reverse().width(l).reverse() # M <- Multiplicand
    q = b_.reverse().width(l).reverse() # Q <- Multiplier
    q_1 = Bits("0")                     # Q_-1 <- 0
    count = l                           # Count <- n
    doc.line(r"We want to find the result of %s $\cdot$ %s." % (m, q))

    doc.line(r"\begin{align*}")
    doc.line(r"A &\gets %s \\" % a)
    doc.line(r"Q &\gets %s \\" % q)
    doc.line(r"Q_{-1} &\gets %s \\" % q)
    doc.line(r"M &\gets %s \\" % m)
    doc.line(r"\end{align*}")

    doc.line(r"\begin{table}[H]")
    doc.line(r"\begin{tabular}{ccccl}")
    doc.line(r"  $A$ & $Q$ & $Q_{-1}$ & $M$ & Notes \\ \hline \\")
    doc.line(r"  %s & %s & %s & %s & %s \\" % (a, q, q_1, m, "Initial values"))
    while count > 0:
        # Q0, Q-1
        qq = (q[-1], q_1[0])
        if qq == (True, False):
            # Q0, Q-1 = 1,0
            # A <- A - M
            a = add_twos_complement(a, negate_twos_complement(m))
            note = "$1,0 \Rightarrow A \gets A - M$ "
            doc.line(r"  %s & %s & %s & %s & %s \\" % (a, q, q_1, m, note))
        elif qq == (False, True):
            # Q0, Q-1 = 0,1
            # A <- A + M
            a = add_twos_complement(a, m)
            note = "$0,1 \Rightarrow A \gets A + M$ "
            doc.line(r"  %s & %s & %s & %s & %s \\" % (a, q, q_1, m, note))
        else:
            #Q0, Q-1 = 0,0 or 1,1 - do nothing
            qq_str = "0,0" if (False, False) else "1,1"
            note = "$%s$ -- Only shift" % qq_str
            doc.line(r"  %s & %s & %s & %s & %s \\" % (a, q, q_1, m, note))

        # Arithmetic shift right A,Q,Q-1
        aqq = Bits(a.bits + q.bits + q_1.bits)
        aqq.shift_right(arithmetic=True)

        (a, q, q_1) = tuple(aqq.chunks(l))
        q_1.width(1)

        note = "Arithmetic shift right"
        doc.line(r"  %s & %s & %s & %s & %s \\" % (a, q, q_1, m, note))

        # Count <- Count - 1
        count -= 1
    doc.line(r"\end{tabular}")
    doc.line(r"\end{table}")

    
    aq = Bits(a.bits + q.bits)
    doc.line(r"The resut is in A,Q: %s" % (aq))
    return aq

# TODO: Implement elementary school long division with binary
# Note: Might not be implemented. Use division alg below.
def binary_division(a, b, doc=vdoc):
    pass

# Implementation of algorithm described as a flow chart
# on page 98 in slide 2.
def binary_division_alg(a_, b_, doc=vdoc):
    l = max(len(a_), len(b_))
    a = Bits().width(l)                 # All zeros
    q = a_.reverse().width(l).reverse() # Q <- Dividend
    m = b_.reverse().width(l).reverse() # M <- Divisor
    count = l                           # Count <- n
    doc.line(r"We want to find the quotient and remainder of %s / %s." % (q, m))

    doc.line(r"\begin{align*}")
    doc.line(r"A &\gets %s \\" % a)
    doc.line(r"Q &\gets %s \\" % q)
    doc.line(r"M &\gets %s \\" % m)
    doc.line(r"\end{align*}")

    doc.line(r"\begin{table}[H]")
    doc.line(r"\begin{tabular}{cccl}")
    doc.line(r"  $A$ & $Q$ & $M$ & Notes \\ \hline \\")
    doc.line(r"  %s & %s & %s & %s \\" % (a, q, m, "Initial values"))
    while count > 0:
        # Combine A,Q, shift left and update A,Q
        aq = Bits(a.bits + q.bits)
        aq.shift_left()
        a, q = tuple(aq.chunks(l))

        note = "Shift left"
        doc.line(r"  %s & %s & %s & %s \\" % (a, q, m, note))

        # A <- A - M
        a = add_twos_complement(a, negate_twos_complement(m))
        note = "$A \gets A - M$"
        doc.line(r"  %s & %s & %s & %s \\" % (a, q, m, note))

        # If A < 0
        if a.lsb() == True:
            # Q0 <- 0
            # A <- A + M
            q.bits[-1] = False
            a = add_twos_complement(a, m)
            note = "$A < 0$, $Q_{0} \gets 0$, $A \gets A + M$"
            doc.line(r"  %s & %s & %s & %s \\" % (a, q, m, note))
        else:
            # Q0 <- 1
            q.bits[-1] = True
            note = "$A \geq 0$, $Q_{0} \gets 1$"
            doc.line(r"  %s & %s & %s & %s \\" % (a, q, m, note))

        # Count <- Count - 1
        count -= 1
    doc.line(r"\end{tabular}")
    doc.line(r"\end{table}")

    doc.line(r"Quotient in Q: %s" % (q))
    doc.skip()
    doc.line(r"Remainder in A: %s" % (a))
    return (q, a)



def float_to_binary(f, doc=vdoc):
    pass

def binary_to_float(bits, doc=vdoc):
    pass

def float_decimal_addition(a, b, doc=vdoc):
    pass

def float_decimal_multiplication(a, b, doc=vdoc):
    pass
