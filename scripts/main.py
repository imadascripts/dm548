#!/usr/bin/env python3

from bits import *
from binary import *
from latex import *

doc = LatexDocument("document")

a = Bits("0111")
b = Bits("0011")

binary_multiply_booth(a, b, doc)

doc.pagebreak()

binary_division_alg(a, b, doc)

doc.compile()
