# HOWTO

This requires Python 3 and pdflatex (if you want to compile the LaTeX document)

Edit main.py with the desired functions and run

python3 main.py




## Functions

### binary.py

- `binary_to_unsigned_decimal_general`: Takes no arguments. 

Prints general information about how to convert
a binary digits to decimal interpreted unsigned.


- `binary_to_unsigned_decimal`: Takes binary number, returns decimal

Converts binary digits to decimal (base 10)
interpreting as unsigned.


- `add_twos_complement`: Takes two binary numbers and adds them

- `negate_twos_complement`: Takes binary number, return binary number

Given a binary number in twos complement,
it negates the number such that corresponding 
decimal number a becomes -a.


- `binary_to_twos_complement`: Takes binary number, return decimal

Given a binary number in twos complement,
it converts the number to decimal.


- `binary_to_hexidecimal`: Takes binary number, returns hexidecimal

- `decimal_to_binary`: Takes decimal, returns binary number

- `decimal_to_twos_complement`: Takes signed decimal, returns binary number 

Given a decimal number (base 1), 
it converts it to a binary number 
which can be interpreted as twos complement to get back to decimal


- `binary_multiply_booth`: Takes two binary numbers, returns binary number

Performs Booth's algorithm (multiplication) 
on two binary numbers in twos complement. 
The result is number of bits in input 1 + 
number of bits in input 2 long.
This algorithm is described as a flow chart
on page 39 in slide 2.


- `binary_division_alg`: Takes two binary numbers, returns binary quotient and remainder

Performs long division on two binary numbers 
and returns the quotient and remainder.
This algorithm is described as a flow chart
on page 98 in slide 2.



## LaTeX Document

The LaTeX document has a couple of interesting methods:

- `doc.line(r"Some line")`

Adds a line to the document

- `doc.skip()`

Adds a \medskip to the document.
Can be changed to small or big skip
with doc.skip("small") or doc.skip("big")

- `doc.pagebreak()`

Adds a \newpage to the document

- `doc.linebreak()`

Adds a linebreak to the document

- `doc.compile()`

Compiles the document using pdflatex

- `doc.to_file()`

Writes the document to a file

- `doc.render()`

Makes the source code for the document and returns it
