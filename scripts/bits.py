class Bits:
    """
    Implementation of bit string
    Note: This is implemented as little endian,
    so all results are read from left to right,
    however normally bit strings are read from 
    right to left (001 is 1, not 4, usually).
    The user is responsible for reversing the input 
    and output.
    """
    def __init__(self, bits = None, *args):
        self.bits = []
        if isinstance(bits, str):
            for c in bits:
                if c == '1':
                    self.bits.append(True)
                elif c == '0':
                    self.bits.append(False)
        elif isinstance(bits, list):
            self.bits = bits


    def __str__(self):
        def conv(bit):
            if bit:
                return '1'
            else:
                return '0'
        s = [conv(x) for x in self.bits]
        return ''.join(s)

    def __repr__(self):
        return self.__str__()
    def __unicode__(self):
        return self.__str__()

    def __len__(self):
        return len(self.bits)

    def __contains__(self, x):
        return x in self.bits

    def __getitem__(self, key):
        if isinstance(key, slice):
            return self.slice(key)
        elif isinstance(key, int):
            #if 0 <= key < len(self):
            return self.bits[key]
            #else: 
            #    return self.__missing__(key)
        print("Invalid key '%s'" % key)
    
    #def __missing__(self, key):
    #    return 0

    def reverse(self):
        bits = self.bits[::-1]
        return Bits(bits)

    def lsb(self):
        return self[0]
    
    def msb(self):
        return self[-1]

    def slice(self, s):
        all_bits = self.bits
        bits = [all_bits[x] for x in range(*s.indices(len(all_bits)))]
        return Bits(bits)
    
    def chunks(self, size=8):
        l = len(self)
        res = []
        for i in range(0, len(self), size):
            m = self[i:i+size]
            if len(m) < size:
                m.width(size)
            res.append(m)
        return res

    def width(self, size):
        l = len(self)
        if l > size:
            self.bits = self.bits[:size]
        elif l < size:
            self.pad_right(size - l)
        return self # For chaining

    def pad_right(self, amount, element=False):
        self.bits.extend([element] * amount)
        return self # For chaining

    def pad_left(self, amount, element=False):
        self.bits = ([element] * amount) + self.bits
        return self # For chaining

    def flip(self):
        bits = list(map(lambda x: not(x), self.bits))
        return Bits(bits)

    def __add__(self, other, truncate=True):
        # Initiate an empty list of result bits
        bits = []
        # Store the length of the two operands
        len_self = len(self)
        len_other = len(other)
        # Find the largest
        m = max(len_self, len_other) + 1
        # And pad bits to be of that size
        self.width(m)
        other.width(m)

        # Start the addition
        # Carry is 0 to start off
        carry = False
        for i in range(m):
            # Get the current bits
            b1 = self[i]
            b2 = other[i]
            # Perform a full-adder
            x1 = b1 != b2     # First half-adder
            a1 = b1 and b2    #
            x2 = x1 != carry  # Second half-adder
            a2 = x1 and carry #
            carry = a1 or a2  # Carry
            bits.append(x2)
        # Restore the lengths
        self.width(len_self)
        other.width(len_other)
        # Create a new bit string based on the result
        # and truncate it to the correct size
        res = Bits(bits)
        if truncate:
            res = res.width(m-1)
        return res

    def shift_left(self, amount=1, arithmetic=False):
        elem = self[-1] if arithmetic else False
        self.pad_right(amount, elem)
        for i in range(amount):
            self.bits.pop(0)
        return self

    def shift_right(self, amount=1, arithmetic=False):
        elem = self[0] if arithmetic else False
        self.pad_left(amount, elem)
        for i in range(amount):
            self.bits.pop(len(self.bits)-1)
        return self

def bit32(s):
    return Bits(s).width(32)
